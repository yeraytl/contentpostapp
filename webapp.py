import socket

class webapp:

    #Esta parte de aqui en verdad no importa mucho ya que la clase que vamos a usar es contentPostApp que hereda de webapp
    def parse(self, received):
        recibido = received.decode()
        return recibido.split(' ')[1]

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World! Tu peticion es" + analyzed[1] + "</h1>" \
               + "</body></html>"
        return http, html

    def __init__(self, ip, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((ip, port))
        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print(address)
            print("HTTP request received:")
            received = recvSocket.recv(2048).decode('utf-8')
            #print(received)

            petition = self.parse(received)
            http, html = self.process(petition)

            response = "HTTP/1.1"+ http+ "\r\n\r\n" \
            +html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()

if __name__ == "__main__":
    webapp = webapp('localhost', 1234)
