import webapp

class contentPostAPP(webapp.webapp):

    recursos = {'/':'p&aacute;gina principal',
                '/yeray':'p&aacute;gina de yeray',
                '/prueba': 'Aqui tenemos otra de prueba'
                }

    def parse(self, request):
        '''Pruebas de depuración
        print("---------------------------------------")
        print(request)
        print("---------------------------------------")
        '''
        recurso = request.split(' ')[1]
        print("Recurso:", recurso)
        cuerpo = request.split('=')[-1]
        print("Cuerpo: ", cuerpo)
        metodo = request.split(' ')[0]
        print("Metodo:", metodo)

        return recurso, cuerpo, metodo

    def process(self, analyzed):
        recurso, cuerpo, metodo = analyzed

        formulario = "<p>" + "<form action='' method='POST'><p>" \
                     + "Escriba el contenido que desee para esta ruta : <input name = 'cuerpo'>" \
                     + "<input type='submit' value='Enviar' />" \
                     + "</body></html>"
        

        if metodo == "GET":
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta " + recurso + " existe y contiene: " + self.recursos[recurso] + "<p>" \
                           + formulario + '<body><html>'
            else:
                http = "404 Not Found"
                html = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta " + recurso + " no existía pero la acabamos de crear<p>" \
                           + formulario + '<body><html>'
        elif metodo == "PUT":
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>Esta es una zona en pruebas para ampliar el programa "'<body><html>'
            else:
                http = "404 Not Found"
                html = html ="<html><body><img src='https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png'/></body></html>"
        else:
            self.recursos[recurso] = cuerpo
            #print(self.recursos)
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta " + recurso + " ahora contiene: " + self.recursos[recurso] + "<p>" \
                           + formulario + '<body><html>'
            else:
                http = "404 Not Found"
                html = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>La ruta" + recurso + " no existe pero si quieres puedes crear una<p>" \
                           + formulario + '<body><html>'

        return (http, html)

if __name__ == "__main__":
        testWebApp = contentPostAPP("localhost", 1235)
